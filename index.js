const express = require('express')
const app = express()
var port = process.env.PORT || 8080; //IMPforDeployment app.use(express.static('static'))
app.use(express.urlencoded({ extended: false }))
const cors = require('cors')//New for microservice
app.use(cors())


app.listen(port), console.log("ExpressServerRunningonPORT " + port)
//app.get("/", (req, res) => { res.sendFile(__dirname + '/static/index.html')
//});

app.get("/", (req, res) => {res.send("Microservice 1 Gateway by EshasreeMadireddy. Usage:host/getAgefromserver?data=xxxx");});

app.get("/getAgefromserver", (req, res) => { var dateString = req.query.data;
var result = {};

var signs = [{
"Rat": [1924, 1936, 1948, 1960, 1972, 1984, 1996, 2008, 2020]
}, {
"Ox": [1925, 1937, 1949, 1961, 1973, 1985, 1997, 2009, 2021]
}, {
    "Tiger": [1926, 1938, 1950, 1962, 1974, 1986, 1998, 2010, 2022] }, {
    "Rabbit": [1927, 1939, 1951, 1963, 1975, 1987, 1999, 2011, 2023] }, {
    "Dragon": [1928, 1940, 1952, 1964, 1976, 1988, 2000, 2012, 2024] }, {
    "Snake": [1929, 1941, 1953, 1965, 1977, 1989, 2001, 2013, 2025] }, {
    "Horse": [1930, 1942, 1954, 1966, 1978, 1990, 2002, 2014, 2026] }, {
    "Goat": [1931, 1943, 1955, 1967, 1979, 1991, 2003, 2015, 2027] }, {
     "Monkey": [1932, 1944, 1956, 1968, 1980, 1992, 2004, 2016, 2028] }, {
    "Rooster": [1933, 1945, 1957, 1969, 1981, 1993, 2005, 2017, 2029] }, {
    "Dog": [1934, 1946, 1958, 1970, 1982, 1994, 2006, 2018, 2030] }, {
    "Pig": [1935, 1947, 1959, 1971, 1983, 1995, 2007, 2019, 2031] }]

if (dateString == null || dateString == undefined || dateString == "") 
    { 
        console.log("Please select the birthdate");
                } else {
                var date = new Date(dateString); 
                var year = date.getFullYear(); 
                var signFound = false; signs.forEach(function (item) {
                    for (const [key, value] of Object.entries(item)) { 
                        for (i = 0; i < value.length; i++) {
                    if (value[i] == year) {
                        result.sign = "Chinese Zodiac sign is: "+key; signFound = true;
                    }
                     } }
                });

            if (signFound == false) {
            result.sign = "Not Able to Calculate the Sign.";
            }
            if (signFound == true) {
                var now = new Date();
             
                var today = new Date(now.getYear(), now.getMonth(), now.getDate());
                var yearNow = now.getYear(); var monthNow = now.getMonth(); var dateNow = now.getDate();
                var dob = new Date(dateString);
                var yearDob = dob.getYear(); var monthDob = dob.getMonth(); var dateDob = dob.getDate(); var age = {};
                 var ageString = "";
                var yearString = "";
                var monthString = "";
                var dayString = "";

                yearAge = yearNow - yearDob;
                if (monthNow >= monthDob)
                var monthAge = monthNow - monthDob;
                else {
                    yearAge--;
                var monthAge = 12 + monthNow - monthDob; }
                if (dateNow >= dateDob)
                    var dateAge = dateNow - dateDob;
                else {
                    monthAge--;
                var dateAge = 31 + dateNow - dateDob;
                    if (monthAge < 0) {
                        monthAge = 11;
                 yearAge--; }
                }
                age = {
                    years: yearAge,
                    months: monthAge,
                    days: dateAge
                };
             
            if (age.years > 1) yearString = "years"; else yearString = "year";
            if (age.months > 1) monthString = "months"; else monthString = "month";
            if (age.days > 1) dayString = "days"; else dayString = "day";
            if ((age.years > 0) && (age.months > 0) && (age.days > 0))
            ageString = age.years + yearString + ", " + age.months + monthString + ",and " + age.days + dayString + "old.";
            else if ((age.years == 0) && (age.months == 0) && (age.days > 0))

                ageString = "Only " + age.days + dayString + "old!"; 
            else if ((age.years > 0) && (age.months == 0) && (age.days == 0))
                ageString = age.years + yearString + "old.HappyBirday";
            else if ((age.years > 0) && (age.months > 0) && (age.days == 0)) 
                ageString = age.years + yearString + "and " + age.months + monthString + "old.";
            else if ((age.years == 0) && (age.months > 0) && (age.days > 0))
                ageString = age.months + monthString + "and " + age.days + dayString + "old.";
            else if ((age.years > 0) && (age.months == 0) && (age.days > 0)) 
                ageString = age.years + yearString + "and " + age.days + dayString + "old.";
            else if ((age.years == 0) && (age.months > 0) && (age.days == 0))
            ageString = age.months + monthString + "old.";
            else ageString = "Oops!Couldnotcalculateage!";
            result.ageString = ageString; }
            }


res.send(result);
});
//app.post("/echo.php", (req, res) => { res.send(req.body.data)
//})